xml.feed do |feed|
	  feed.title "Incomplete Tasks for #{current_user.email}"
	  feed.updated @todos.maximum(:updated_at)
	  @todos.each do |post|
		    feed.entry do |entry|
		        entry.title post.title
			entry.published post.updated_at
			entry.url "http://todo.d8u.us#{todo_path(post)}"
			entry.author do |author|
				author.name current_user.email
			end
		end # end feed.entry
	  end # end @todos.each
end

