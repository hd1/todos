class Todo < ActiveRecord::Base
	belongs_to :user
	after_initialize :init
	def init
		self.completed = false
	end
	attr_accessor :completed
	validates :title, presence: true

end
