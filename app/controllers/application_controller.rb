class ApplicationController < ActionController::Base
#  acts_as_token_authentication_handler_for User
  # Prevent CSRF attacks by raising an exception.
  protect_from_forgery with: :exception
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session, :if => Proc.new { |c| c.request.format == 'application/json' }
end
