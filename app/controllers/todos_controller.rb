class TodosController < ApplicationController
  before_filter :authenticate_user!
  def daily_mail
	  DailyMailer.dailysend
  end

  def handle_unverified_request
	raise(ActionController::InvalidAuthenticityToken)
  end
  def index
    @todos = Todo.where('user_id = ?', current_user.id).order(updated_at: :desc)
    respond_to do |format|
	    format.json { render :json => @todos }
	    format.atom
	    format.html
    end
  end

  def todo_params
	  params.require(:todo).permit(:title, :user_id)
  end

  def create
    @todo = Todo.new(todo_params)
    @todo.user_id = current_user.id
    @todo.completed = false
    @todo.save
    redirect_to 'index'
  end

  def update
    @todo = Todo.find(params[:id])
    @todo.update(todo_params)
  end

  def show
	  @todo = Todo.where('user_id = ?', current_user.id)
	  respond_to do |format|
		  format.html
		  format.json { render :json => @todo }
	  end
  end

  def destroy
    @todo = Todo.find(params[:id])
    @todo.destroy
  end

  def toggle
    @todo = Todo.find(params[:id])
    @todo.toggle!(:completed)
  end

  def toggle_all
    @todos = Todo.where('user_id = ?', current_user.id)
    @todos.each do |t|
      t.toggle!(:completed)
    end
  end

private

  def todo_params
    params.require(:todo).permit(:title, :completed)
  end

end
