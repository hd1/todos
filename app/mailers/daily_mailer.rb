class DailyMailer < ActionMailer::Base
  default from: "hd1+todo@jsc.d8u.us"
  def dailysend
	  @users = User.all
	  @users.each do |u| 
		  @tasks = Todo.where(user_id:u.id)
		  mail(to: u.email, subject: 'Things to do today')
	  end
  end
end
