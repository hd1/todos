Todos::Application.routes.draw do
  get "welcome/log"
  get 'welcome/index'
  devise_for :users
  resources :todos do
    member do
      post :toggle
    end

    collection do
      post :toggle_all
      get :active
      get :completed
      delete :destroy_completed
    end
  end


  get '/check', to: 'todos#check_password_for'
  root :to => 'welcome#index'
end
